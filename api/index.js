const {queueCreate, queueDelete, queueFindOne, queueUpdate, queueView} = require('../src/Adapters/index')

const Create = async ({name, age, color}) => {
    try {
        const job = await queueCreate.add({name, age, color})

        const {statusCode, data, message} = await job.finished()

        if (statusCode === 200) {
            console.log('Bienvenido', data.name);
        } else {
            console.error(message);
        }

        console.log({statusCode, data, message});

    } catch (error) {
        
        console.log(error);
    }
    
}

const Delete = async ({ id }) => {
    try {
        const job = await queueDelete.add({ id })

        const {statusCode, data, message} = await job.finished()

        if (statusCode === 200) {
            console.log('Usuario eliminado es: ', data.name);
        } else {
            console.error(message);
        }

        console.log({statusCode, data, message});

    } catch (error) {
        
        console.log(error);
    }
    
}

const FindOne = async ({ id }) => {
    try {
        const job = await queueFindOne.add({ id })

        const {statusCode, data, message} = await job.finished()

        if (statusCode === 200) {
            console.log('Usuario buscado es: ', data);
        } else {
            console.error(message);
        }

        console.log({statusCode, data, message});

    } catch (error) {
        
        console.log(error);
    }
    
}

const Update = async ({ name, age, color, id }) => {
    try {
        const job = await queueUpdate.add({ name, age, color, id })

        const {statusCode, data, message} = await job.finished()

        console.log({statusCode, data, message});

    } catch (error) {
        
        console.log(error);
    }
    
}

const View = async ({}) => {
    try {
        const job = await queueView.add({})

        const {statusCode, data, message} = await job.finished()

        if (statusCode === 200) for(let x of data) console.log(x);

        else(message);

    } catch (error) {
        
        console.log(error);
    }
    
}

const main = async() => {
    await Create({name: "Jehan ", age: 255, color: 'Gris'});

    await Update({name: 'crazi', age: 23, color: 'grey', id: 6})

    await FindOne({ id: 6 })

    await Delete({ id: 6 })

    await View({});
}

main();