const Services  = require('../Services')
const { InternalError } = require('../settings')

const { queueCreate, queueDelete, queueFindOne, queueUpdate, queueView } = require('./index')

const Create = async(job, done) => {
    try {
        const { name, age, color } = job.data;

        let { statusCode, data, message } = await Services.Create({ name, age, color });

        done(null, { statusCode, data, message });
        

    } catch (error) {

        console.log({ step: 'Adapters queueView', error: error.toString()});
        
        done({ statusCode: 500, message: InternalError }, null);
    }

}

const Delete = async(job, done) => {
    try {
        const { id } = job.data;

        let { statusCode, data, message } = await Services.Delete({ id });

        done(null, { statusCode, data, message });
        

    } catch (error) {

        console.log({ step: 'Adapters queueView', error: error.toString()});
        
        done({ statusCode: 500, message: InternalError }, null);
    }

}

const FindOne = async(job, done) => {
    try {
        const { id } = job.data;

        let { statusCode, data, message } = await Services.FindOne({ id });

        done(null, { statusCode, data, message });
        

    } catch (error) {

        console.log({ step: 'Adapters queueView', error: error.toString()});
        
        done({ statusCode: 500, message: InternalError }, null);
    }

};

const Update = async(job, done) => {
    try {
        const { name, age, color, id } = job.data;

        let { statusCode, data, message } = await Services.Update({ name, age, color, id });

        done(null, { statusCode, data, message });
        

    } catch (error) {

        console.log({ step: 'Adapters queueView', error: error.toString()});
        
        done({ statusCode: 500, message: InternalError }, null);
    }

};

const View = async(job, done) => {
    try {
        const { } = job.data;

        let { statusCode, data, message } = await Services.View({ });

        done(null, { statusCode, data, message });
        

    } catch (error) {

        console.log({ step: 'Adapters queueView', error: error.toString()});
        
        done({ statusCode: 500, message: InternalError }, null);
    }

};

const run = async() => {
    try {
        
        console.log("inicializar worker");

        queueCreate.process(Create)

        queueDelete.process(Delete)

        queueFindOne.process(FindOne)

        queueUpdate.process(Update)

        queueView.process(View)
        

    } catch (error) {
        console.log(error);
    }
}

module.exports = {
    Create, 
    Delete, 
    FindOne, 
    Update, 
    View,
    run
}
