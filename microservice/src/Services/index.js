const Controllers = require('../Controllers');
const { InternalError } = require('../settings');

 const Create = async({ age, color, name }) => {
    try {

        let { statusCode, data, message } = await Controllers.Create({ age, color, name });

        return { statusCode, data, message };
        

    } catch (error) {

        console.log({ step: 'Services Create', error: error.toString() });
        
        return { statusCode: 500, message: error.toString() };
    }
}

const Delete = async({ id }) => {
    try {

        const findOne = await Controllers.FindOne({ id })

        if (findOne.statusCode !== 200) {
            
            let response = {
                400: { statusCode: 400, message:"no existe el usuario a eliminar" },
                500: { statusCode: 400, message: InternalError }
            }

            return response[findOne.statusCode]

        }
        
        const del = await Controllers.Delete({ where: { id } });
        
        if(del.statusCode === 200) return {statusCode: 200, data: findOne.data}

        return { statusCode: 400, message: InternalError };
        

    } catch (error) {

        console.log({ step: 'Services Delete', error: error.toString() });
        
        return { statusCode: 500, message: error.toString() };
    }
}

const Update = async({ age, color, name, id }) => {
    try {

        let { statusCode, data, message } = await Controllers.Update({ age, color, name, id })

        return { statusCode, data, message }
        

    } catch (error) {

        console.log({ step: 'Services Update', error: error.toString() });
        
        return { statusCode: 500, message: error.toString() }
    }
}

const FindOne = async({ id }) => {
    try {

        let { statusCode, data, message } = await Controllers.FindOne({ id })

        return { statusCode, data, message }
        

    } catch (error) {

        console.log({ step: 'Services FindOne', error: error.toString() });
        
        return { statusCode: 500, message: error.toString() }
    }
}

const View = async({ }) => {
    try {

        let { statusCode, data, message } = await Controllers.View({ })

        return { statusCode, data, message }
        

    } catch (error) {

        console.log({ step: 'Services View', error: error.toString() });
        
        return { statusCode: 500, message: error.toString() }
    }
}

module.exports = {
    Create,
    Delete,
    Update,
    FindOne, 
    View
}