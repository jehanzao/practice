const { Model } = require('../Models')

const Create = async({name, age, color}) => {
    try {
        
        let instance = await Model.create(
            {name, age, color}, 
            {fields: ['name', 'age', 'color'], logging: false}
            );

        return {statusCode: 200, data: instance.toJSON()}

    } catch (error) {
        console.log({step: 'Controller Create', error: error.toString()});
        return({ statusCode: 500, message: error.toString()})        
    }
}

const Delete = async({ where = {} }) => {
    try {
        await Model.destroy({where, logging: false});

        return {statusCode: 200, data: "Ok"}

    } catch (error) {
        console.log({step: 'Controller Delete', error: error.toString()});
        return({ statusCode: 500, message: error.toString()})        
    }
}

const Update = async({name, age, color, id}) => {
    try {

        let instance = await Model.update(
            {name, age, color}, 
            {where: { id }, logging: false, returning: true }
        );

        return {statusCode: 200, data: instance[1][0].toJSON()}

    } catch (error) {
        
        console.log({step: 'Controller Update', error: error.toString()});
        return({ statusCode: 500, message: error.toString()})        
    }
}

const FindOne = async( { id } ) => {
    try {
        let instance = await Model.findOne({ where: { id }, logging: false });

        if (instance) return { statusCode: 200, data: instance.toJSON()}

        else return { statusCode: 400, message: "usuario no encontrado"}

    } catch (error) {

        console.log({step: 'Controller FindOne', error: error.toString()});

        return({ statusCode: 500, message: error.toString()})        
    }
}

const View = async(where = {}) => {
    try {
        let instances = await Model.findAll({ where, logging: false });
        console.log(instances);
        return ({ statusCode: 200, data: instances})
    } catch (error) {
        console.log({step: 'Controller View', error: error.toString()});
        return({ statusCode: 500, message: error.toString()})        
    }
}


module.exports = {
    Create,
    Delete,
    Update,
    FindOne,
    View
}