const {sequelize} = require('../settings')
const { DataTypes } = require('sequelize')

const Model = sequelize.define('curso', {
    name: {type: DataTypes.STRING},
    age: {type: DataTypes.BIGINT},
    color: {type: DataTypes.STRING}
})

const SyncDB = async() => {
    try {
        console.log('inicializando base de datos');

        await Model.sync({logging: false})

        console.log('base de datos inicializada');

        return { statusCode: 200, data: 'ok' }

    } catch (error) {
        console.log(error);
        return { statusCode: 500, message: error.toString()  }
    }
}

module.exports = {SyncDB, Model}