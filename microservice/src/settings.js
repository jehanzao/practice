const dotenv = require('dotenv');
const { Sequelize } = require('sequelize')

dotenv.config();

const redis = {
    host: process.env.REDIS_HOST,
    port: process.env.REDIS_PORT
}

const db = {
    host: process.env.POSTGRES_HOST,
    port: process.env.POSTGRES_PORT,
    username: process.env.POSTGRES_USER,
    database: process.env.POSTGRES_DB,
    password: process.env.POSTGRES_PASSWORD
}

const InternalError = 'Unexpected Error'

const sequelize = new Sequelize({
    host: db.host,
    port: db.port,
    database: db.database,
    username: db.username,
    password: db.password,
    dialect: 'postgres'
})

module.exports = { redis, InternalError, sequelize }