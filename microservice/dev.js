const micro = require('./src/index')

const main = async() => {
    try {

    const db = await micro.SyncDB();  
    
    if (db.statusCode !== 200) throw db.message

    micro.run()

    } catch (error) {
        console.log(error);
    }
}

main()

